Repository for Tox21 human toxicity classification on the MetaCentrum supercomputer.



Descriptor generation is handled by scripts `src/descriptor_generation/*.py`

Hyperparameter tuning of deep NNs: `src/DL/Tox21_tuner.py`

Tuning of GBDT, SVMs, AdaBoost: `src/ML/ML.py`